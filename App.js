import React from 'react';
import { StateProvider } from './src/hooks/state'
import MainPage from './src/index'


const App = () => {
  const pallete = {
    light : {
      primary: '#ed6363',
      primary_variant: '',
      secondary: '',
      secondary_variant: '',
      background: '#e0ecf0',
      surface: '#245d72',
      error: '',
      on_primary: '#dedede',
      on_secondary: '',
      on_background: '#122e35',
      on_surface: '#fefefe',
      on_error: ''
    },
    dark: {
      primary: '#ed6363',
      primary_variant: '#00454a',
      secondary: '#3c6562',
      secondary_variant: '#6b778d',
      background: '#003545',
      surface: '#164a5d',
      error: '',
      on_primary: '#dedede',
      on_secondary: '',
      on_background: '#e0ecf0',
      on_surface: '#fefefe',
      on_error: ''
    }
  }

  const initialState = {
    theme : 'dark',
    color : pallete['dark']
  };

  const reducer = (state, action) => {
    switch (action.type) {
      case 'CHANGE_THEME':
        return { ...state, theme : action.newTheme, color : pallete[action.newTheme] }
      default:
        return state;
    }
  }

  return (
    <StateProvider initialState={initialState} reducer={reducer}>
     <MainPage/>
    </StateProvider>
  )
}

export default App;
