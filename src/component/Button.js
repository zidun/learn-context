import React from 'react';
import { useStateValue } from '../hooks/state';
import { TouchableOpacity, Text, View } from 'react-native';

const Button = () => {
  const [{ theme }, dispatch] = useStateValue();

  return(
    <View style={{ flexDirection: 'row', alignItems: 'center' }}>
      <TouchableOpacity 
        disabled={theme === 'dark'}
        style={{
          backgroundColor: '#122e35',
          paddingVertical: 8, 
          paddingHorizontal: 12,
          borderRadius: 8,
          marginHorizontal: 4,
          borderWidth:1,
          borderColor: '#e0ecf0'
        }}
        onPress={() => {
          dispatch({
            type : 'CHANGE_THEME',
            newTheme : 'dark'
          })
        }}
      >
        <Text style={{ color: '#e0ecf0', fontWeight: 'bold', fontSize: 14, textAlign: 'center'}}>DARK</Text>
      </TouchableOpacity>

      <TouchableOpacity 
        disabled={theme === 'light'}
        style={{
          backgroundColor: '#e0ecf0',
          paddingVertical: 8, 
          paddingHorizontal: 12,
          borderRadius: 8,
          marginHorizontal: 4,
          borderWidth:1,
          borderColor: '#122e35'
        }}
        onPress={() => {
          dispatch({
            type : 'CHANGE_THEME',
            newTheme : 'light'
          })
        }}
      >
        <Text style={{ color: '#122e35', fontWeight: 'bold', fontSize: 14, textAlign: 'center'}}>LIGHT</Text>
      </TouchableOpacity>
    </View>
  )
} 

export default Button;