import React, { useEffect, useState } from 'react'
import {
  View,
  Text,
  Image,
  FlatList,
  ActivityIndicator,
  Platform,
  Dimensions
} from 'react-native'
import axios from 'axios'
import { useStateValue } from '../hooks/state'
const WP = Dimensions.get('screen').width
const HP = Dimensions.get('screen').height

const getDataAPI = (initialURL, initialData) => {
  const [data, setData] = useState(initialData)
  const [url, setUrl] = useState(initialURL)
  const [isLoading, setIsLoading] = useState(false)
  const [isError, setIsError] = useState(false)

  useEffect(() => {
    const fetchData = async () => {
      setIsError(false);
      setIsLoading(true);
      
      try {
        const result = await axios(url)
        setData(result.data.results)
      } catch (error) {
        setIsLoading(true)
      }
      setIsLoading(false)
    }
    fetchData()
  },[url])

  return [{ data, isLoading, isError }, setUrl]
}

const Anime = () => {
  const [{ color, theme } ] = useStateValue();

  const [search, setSearch] = useState('naruto')
  const [{ data, isLoading, isError}, doGetData] = getDataAPI(
    'https://api.jikan.moe/v3/search/anime?q=naruto&limit=100',
    { anime: [] }
  )
  return isError ? (
   <Text style={{ fontWeight: '500', fontSize: 14, textAlign: 'center' }}>Gagal Mengambil Data</Text>
  ) : (
    <View>
      {
        isLoading ? (
          <ActivityIndicator color={color.on_background}/>
        ) : (
          <FlatList
            showsVerticalScrollIndicator={false}
            // pagingEnabled={true}
            data={data}
            // horizontal={true}
            contentContainerStyle={{ paddingTop: 16}}
            numColumns={2}
            keyExtractor={(item, i)=> i.toString()}
            renderItem={ ({item, index}) => 
              <View style={{ 
                elevation:5,
                width: WP/2.25,
                overflow:'hidden',
                marginLeft: index+1 % 2 !== 0 ? 8 : 0,
                marginRight: index+1 % 2 !== 0 ? 8 : 0,
                backgroundColor: color.surface,
                marginBottom: 16,
                borderRadius: 8,
                // height:HP/2.5,
              }}
              >
                <View>
                  <Image style={{ width: WP/2.25, aspectRatio:1, }} source={{ uri: item.image_url}}/>
                  <Text style={{backgroundColor:'rgba(0,0,0,0.9)', position:'absolute',right:0,top:0, paddingVertical:4,paddingHorizontal:8,fontSize:12,color:'white'}}>{item.score}</Text>
                </View>
                <View style={{ padding: 8}}>
                  <Text numberOfLines={2} style={{color: color.on_surface, fontSize: 14, fontWeight: 'bold', marginBottom: 2 }}>{item.title}</Text>
                  <Text numberOfLines={3} style={{color: color.on_primary, fontSize: 10, fontWeight: 'normal', }}>{item.synopsis}</Text>
                </View>
              </View>
            }
          />
        )
      }
    </View>
  )
}

export default Anime;