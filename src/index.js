import React from 'react';
import {
  View,
  Text,
  StatusBar,
  ScrollView
} from 'react-native';
import Button from './component/Button';
import { useStateValue } from './hooks/state';
import Anime from './component/Anime';

const MainPage = () => {
  const [{ color, theme } ] = useStateValue();
  return(
    <View style={{
      flex: 1,
      backgroundColor: color.background,
    }}>
      <StatusBar backgroundColor={color.background} barStyle={`${theme == 'dark' ? 'light' : 'dark'}-content`}/>
      <View style={{ paddingVertical: 8, paddingHorizontal: 16, alignItems: 'center', flexDirection: 'row', justifyContent: 'space-between' }}>
        <Text style={{ fontSize: 24, fontWeight: 'bold', textAlign: 'left', color: color.primary }}>Anime</Text>
        <Button/>
      </View>
      <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
        <Anime/>
      </View>
    </View>
  )
}

export default MainPage;